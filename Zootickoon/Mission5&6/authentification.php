<html>
    <head>
       <meta charset="utf-8">
       <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
       <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
       <link rel='stylesheet' type='text/css' href='style.css' media='all'>
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    </head>
    <body>
    
    <script>
    function affMail() {
       $(document).ready(function(){ var email= $( "#mail" ).val(); alert(email); }); }
    </script>

    <header>
            <div>
                <img src="images/logo/Zootickoon.png" alt="logo" class="logo">
            </div><br>
            <ul class="menu">
                <li><a href="index.php">Home</a></li>
                <li><a href="#">Accomodation</a></li>
                <li><a href="#">Products</a></li>
                <li><a href="#">Activities</a></li>
                <?php 
                session_start();
                if(isset($_SESSION['email']))  { 
                 echo '<li><a href="formTicket.php">Ticket</a></li>';
                 echo '<li><a href="deco.php">Deconnection</a></li>';
                  } 
                  else { 
                 echo '<li><a href="authentification.php" class="active">Account</a></li>';
                    }
                ?>
              </ul>
        </header><hr>

        <div class="encadre">
        <div id="container">
            
            <form action="verification.php" method="POST" class="centre">
                <h1>Connection</h1>
                
                <div class="mb-3">
                    <label class="form-label"><b>Email</b></label>
                    <input type="email" placeholder="Entrer votre email" name="email" required>
                </div>

                <div class="mb-3">
                    <label class="form-label"><b>Password</b></label>
                    <input type="password" placeholder="Entrer le mot de passe" name="password" required>
                </div>

                <div>
                <input type="submit" class="btn btn-primary" value='LOGIN' >
                </div>

                <?php
                if(isset($_GET['erreur'])){
                    $err = $_GET['erreur'];
                    if($err==1 || $err==2)
                        echo '<br> <div class="alert alert-danger" role="alert"> Incorrect email or password.</div>';
                }
                
                function alert($msg) {
                    echo "<script type='text/javascript'>alert('$msg');</script>";
                }

                if(isset($_GET['email'])){
                    $mel = $_GET['email'];
                    alert("Connected $mel");
                      
                    echo '<br> <div class="alert alert-success" role="alert"> Connected ! </div>';
                }
                ?>
            </form>  
        </div>
        </div> <br><br>
        
        <div class="encadre">
        <form action="creation.php" method="POST" class="centre">
                <h1>Create my account</h1>
                
                <div class="mb-3">
                    <label class="form-label"><b>Email</b></label>
                    <input type="email" id="mail" placeholder="Entrer votre email" name="email" required>
                </div>

                <div class="mb-3">
                    <label class="form-label"><b>Password</b></label>
                    <input type="password" placeholder="Entrer le mot de passe" name="password" required>
                </div>

                <div>
                  <input type="submit" class="btn btn-primary" value='SIGN UP' onclick="affMail()" >
                </div>
            </form>
            
            <?php
                if(isset($_GET['erreur'])){
                    $err = $_GET['erreur'];
                    if($err==3)
                        echo '<br> <div class="alert alert-danger" role="alert"> Email already used !</div>';
                }
                ?>  
        </div>
    </body>
</html>