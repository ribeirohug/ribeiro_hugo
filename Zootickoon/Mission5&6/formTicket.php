<!DOCTYPE html>
    <html>      
        <head>
            <meta charset="utf-8">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
            <title>Ticket</title>
        </head>
    
        <body>
        <?php 
        session_start();
        if(isset($_SESSION['email']))  {} 
        else { 
        header("Location: authentification.php"); 
        }
      ?>
        <header>
            <nav class="navbar navbar-dark bg-primary">
            <a class="navbar-brand" href="#">Menu</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                <a class="nav-item nav-link" href="afficherListeTicket.php">Ticket <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link active" href="formTicket.php">Add Ticket</a>
                <a class="nav-item nav-link" href="index.php">Home</a>
                </div>
            </div>
            </nav>
        </header> <br><br>

        <div class="container">
        <form action="addTicket.php" method="POST" class = 'card p-3 bg-light'>
                <div class="mb-3">
                    <label for="Priority">Priority Level</label>
                        <select name="Priority" id="Priority">
                            <option value="">--Please choose an option--</option>
                            <option value="High">High</option>
                            <option value="Medium">Medium</option>
                            <option value="Low">Low</option>
                        </select>
                </div> <br>

                <div class="mb-3">
                  <label for="Subject">Subject</label>
                  <input type="text" id="Subject" name="Subject" placeholder="Enter a subject">
                </div> <br>

                <div class="mb-3">
                    <label for="Description">Description</label>
                    <textarea id="Description" name="Description" placeholder="Enter a description for this issue."></textarea>
                  </div> <br>

                  <div class="mb-3">
                    <label for="Sector">Sector</label>
                        <select name="Sector" id="Sector">
                            <option value="">--Please choose an option--</option>
                            <option value="Bear">Bear</option>
                            <option value="Pinguin">Pinguin</option>
                            <option value="Seal">Seal</option>
                            <option value="Wolf">White wolf</option>
                        </select>
                </div> <br>

                <div class="mb-3">
                    <label for="Login">Your login</label>
                    <input type="text" id="Login" name="Login" placeholder="Type your login">
                  </div> <br>

                <button type="submit" value="submit" class="btn btn-primary">Submit</button>
              </form> 
            </div>

            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        </body>
    
    </html>