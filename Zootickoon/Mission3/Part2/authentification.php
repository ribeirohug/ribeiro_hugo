<html>
    <head>
       <meta charset="utf-8">
       <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
       <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    </head>
    <body>

        <div id="container">
            
            <form action="verification.php" method="POST">
                <h1>Connexion</h1>
                
                <div class="mb-3">
                    <label class="form-label"><b>Email</b></label>
                    <input type="email" placeholder="Entrer votre email" name="email" required>
                </div>

                <div class="mb-3">
                    <label class="form-label"><b>Mot de passe</b></label>
                    <input type="password" placeholder="Entrer le mot de passe" name="password" required>
                </div>

                <div>
                <input type="submit" class="btn btn-primary" value='LOGIN' >
                </div>


                <?php
                if(isset($_GET['erreur'])){
                    $err = $_GET['erreur'];
                    if($err==1 || $err==2)
                        echo '<br> <div class="alert alert-danger" role="alert"> Incorrect email or password.</div>';
                }
                
                function alert($msg) {
                    echo "<script type='text/javascript'>alert('$msg');</script>";
                }

                if(isset($_GET['email'])){
                    $mel = $_GET['email'];
                    alert("Connected $mel");
                    echo '<br> <div class="alert alert-success" role="alert"> Connected ! </div>';
                }
                ?>
                
            </form>
        </div>
    </body>
</html>